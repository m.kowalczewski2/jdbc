package com.sda.model.mapper;

import com.sda.model.Country;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryMapper {

    public Country mapResultSetToCountry(ResultSet resultSet) throws SQLException {
        return new Country(
                resultSet.getInt("CO_ID"),
                resultSet.getString("CO_NAME"),
                resultSet.getString("CO_ALIAS")
        );
    }

}
