package com.sda.zadanie2;

import com.sda.dao.CountryDao;
import com.sda.dao.CountryDaoImpl;
import com.sda.model.Country;
import com.sda.model.mapper.CountryMapper;

import java.sql.SQLException;

public class Zadanie2 {

    public static void main(String[] args) throws SQLException {

        CountryDao countryDao = new CountryDaoImpl(new CountryMapper());

        //countryDao.getAll().forEach(System.out::println);

        //countryDao.updateCountry(new Country(6, "Portugal", "PR"));
        countryDao.findById(10).ifPresent(System.out::println);
        countryDao.addNewCountry(new Country("test", "test"));

    }

}
