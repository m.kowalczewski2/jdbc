package com.sda.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    private final String url= "jdbc:mysql://localhost:3306/shop?serverTime=CET";
    private final String user = "root";
    private final String password = "root";

    private static DatabaseConnection databaseConnection;
    private Connection connection;

    private DatabaseConnection() {
        try {
            this.connection = DriverManager.getConnection(url, user, password);
            System.out.println("Connected");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static DatabaseConnection getDatabaseConnection() {
        if (databaseConnection == null) {
            databaseConnection = new DatabaseConnection();
        } else {
            try {
                if (databaseConnection.connection.isClosed()) {
                    databaseConnection = new DatabaseConnection();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return databaseConnection;
    }

    public Connection getConnection() {
        return connection;
    }
}
