package com.sda.dao;

import com.sda.model.Country;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface CountryDao {

    List<Country> getAll() throws SQLException;
    Optional<Country> findById(int id) throws SQLException;
    void addNewCountry(Country country) throws SQLException;
    void deleteById(int id) throws SQLException;
    void updateCountry(Country country) throws SQLException;

}
