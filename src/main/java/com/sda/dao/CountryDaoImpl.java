package com.sda.dao;

import com.sda.model.Country;
import com.sda.model.mapper.CountryMapper;
import com.sda.util.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CountryDaoImpl implements CountryDao {

    public static final int NAME_UPDATE_PARAMETER_INDEX = 1;
    public static final int ALIAS_UPDATE_PARAMETER_INDEX = 2;
    public static final int ID_UPDATE_PARAMETER_INDEX = 3;
    public static final int ID_FIND_PARAMETER_INDEX = 1;
    public static final int NAME_INSERT_PARAMETER_INDEX = 1;
    public static final int ALIAS_INSERT_PARAMETER_INDEX = 2;
    public static final int DELETE_PARAMETER_INDEX = 1;

    private final CountryMapper countryMapper;
    private final String selectAllQuery = "select * from Country";
    private final String findByIdQuery = "select * from Country where co_id = ?";
    private final String insertQuery = "insert into Country (CO_NAME, CO_ALIAS) values (?, ?)";
    private final String updateQuery = "UPDATE Country " +
            "SET CO_NAME = ?, CO_ALIAS = ? " +
            "WHERE CO_ID = ?";
    private final String deleteById = "delete from Country where CO_ID = ?";

    public CountryDaoImpl(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    @Override
    public List<Country> getAll() throws SQLException {
        ResultSet resultSet = getConnection().createStatement().executeQuery(selectAllQuery);
        List<Country> countries = new ArrayList<>();
        while (resultSet.next()) {
            countries.add(countryMapper.mapResultSetToCountry(resultSet));
        }
        getConnection().close();
        return countries;
    }



    @Override
    public Optional<Country> findById(int id) throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(findByIdQuery);
        preparedStatement.setInt(ID_FIND_PARAMETER_INDEX, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Optional<Country> result = Optional.empty();
        if (resultSet.next()) {
            result = Optional.of(countryMapper.mapResultSetToCountry(resultSet));
        }
        getConnection().close();
        return result;
    }

    @Override
    public void addNewCountry(Country country) throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(insertQuery);
        preparedStatement.setString(NAME_INSERT_PARAMETER_INDEX, country.getName());
        preparedStatement.setString(ALIAS_INSERT_PARAMETER_INDEX, country.getAlias());
        preparedStatement.executeUpdate();
        getConnection().close();
    }

    @Override
    public void deleteById(int id) throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(deleteById);
        preparedStatement.setInt(DELETE_PARAMETER_INDEX, id);
        preparedStatement.executeUpdate();
        preparedStatement.getConnection().close();
    }

    @Override
    public void updateCountry(Country country) throws SQLException {
        //Spróbuje napisać to tak, że jeżeli kraj nie istnieje, to dodaj zamiast zaktualizować
        PreparedStatement preparedStatement = getConnection().prepareStatement(updateQuery);
        preparedStatement.setString(NAME_UPDATE_PARAMETER_INDEX, country.getName());
        preparedStatement.setString(ALIAS_UPDATE_PARAMETER_INDEX, country.getAlias());
        preparedStatement.setInt(ID_UPDATE_PARAMETER_INDEX, country.getId());
        if (preparedStatement.executeUpdate() < 1) {
            System.out.println("nic nie zostało zaktualizowane");
        }
        getConnection().close();
    }

    private Connection getConnection() {
        return DatabaseConnection.getDatabaseConnection().getConnection();
    }

}
