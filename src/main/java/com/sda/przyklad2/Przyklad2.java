package com.sda.przyklad2;

import com.sda.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Przyklad2 {

    public static void main(String[] args) throws SQLException {

        String id = "4 or 1=1";
        String query = "Select * from Address where add_id = ?";
        //String query = "Select * from Address where add_id = "+id;

        DatabaseConnection databaseConnection = DatabaseConnection.getDatabaseConnection();
        PreparedStatement preparedStatement = databaseConnection.getConnection().prepareStatement(query);

        preparedStatement.setString(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        while(resultSet.next()) {
            System.out.println("Adress Street is: " + resultSet.getString("ADD_STREET"));
        }

    }

}
