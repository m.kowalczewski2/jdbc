package com.sda.przyklad4;

import com.sda.model.Address;
import com.sda.model.Country;
import com.sda.util.DatabaseConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Przyklad4 {

    public static void main(String[] args) throws SQLException {
        String query = "Select * from Address join Country on co_id=add_co_id";

        DatabaseConnection databaseConnection = DatabaseConnection.getDatabaseConnection();

        ResultSet resultSet = databaseConnection.getConnection()
                .createStatement()
                .executeQuery(query);

        List<Address> addresses = new ArrayList<>();

        while (resultSet.next()) {
            addresses.add(mapResultSetToAddress(resultSet));
        }

        addresses.forEach(System.out::println);

        databaseConnection.getConnection().close();
    }

    private static Address mapResultSetToAddress(ResultSet resultSet) throws SQLException {
        return new Address(
                resultSet.getInt("ADD_ID"),
                resultSet.getString("ADD_STREET"),
                resultSet.getString("ADD_BUILDING_NO"),
                resultSet.getString("ADD_APARTAMENT_NO"),
                resultSet.getString("ADD_CITY"),
                resultSet.getString("ADD_POSTAL_CODE"),
                mapResultSetToCountry(resultSet)
        );
    }

    private static Country mapResultSetToCountry(ResultSet resultSet) throws SQLException {
        return new Country(
                resultSet.getInt("CO_ID"),
                resultSet.getString("CO_NAME"),
                resultSet.getString("CO_ALIAS")
        );
    }

}
