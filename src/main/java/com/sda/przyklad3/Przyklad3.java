package com.sda.przyklad3;

import com.sda.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Przyklad3 {

    public static final int BUILDING_NO_PARAMETER_INDEX = 1;
    public static final int POSTAL_CODE_PARAMETER_INDEX = 2;

    public static void main(String[] args) throws SQLException {
        String query = "Update Address "+
                " SET ADD_BUILDING_NO = ?"+
                " WHERE ADD_POSTAL_CODE = ?";

        DatabaseConnection databaseConnection = DatabaseConnection.getDatabaseConnection();
        PreparedStatement preparedStatement = databaseConnection.getConnection()
                .prepareStatement(query);

        preparedStatement.setString(BUILDING_NO_PARAMETER_INDEX, "1");
        preparedStatement.setString(POSTAL_CODE_PARAMETER_INDEX, "97122");

        int numberOfRowsUpdated = preparedStatement.executeUpdate();

        System.out.println("Zaktualizowano "+numberOfRowsUpdated+ " wierszy");


    }

}
